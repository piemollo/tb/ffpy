"""
FFPython init
"""

__all__ = [
    "readmatrix",
    "savematrix",
    "readdata",
    "savedata",
]

from .io import readmatrix, savematrix, readdata, savedata
