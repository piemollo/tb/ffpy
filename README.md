# FreeFEM/Python interfacing

## Description
The module **FFPython** provides Python functions to load 
[FreeFEM](https://freefem.org/) structures. 

## Dependencies and installation

### Installing from source
To install this module, clone this repository with:
```bash
> git clone https://gitlab.com/piemollo/tb/ffpy.git
```

and then install the package in [development mode](https://setuptools.pypa.io/en/latest/userguide/development_mode.html):
```bash
> pip install -e .
```

### Dependencies
To import array/matrix structures this module depends on `numpy` and `scipy`.