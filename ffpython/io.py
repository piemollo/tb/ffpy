"""
Input/Output FreeFEM structures
"""

import numpy as np
from scipy.sparse import coo_matrix


# ====== I/O matrix

def readmatrix(filename):
    """
    Read FreeFEM sparse matrix (COO)
    
    :filename: path to source file
    """

    ## Open file:
    with open(filename,'r') as file:
        filecontent = file.readlines()[2:]

    ## Extract matix size:
    ext_size = filecontent[0].split()
    ni  = int(ext_size[0])
    nj  = int(ext_size[1])
    nnz = int(ext_size[2])

    ## Define coordinates and content vectors:
    II, JJ, VV = [], [], []

    ## Process file content:
    for ii in range(nnz):
        cur = filecontent[ii+1].split()

        ## Store current coordinates and float value:
        if abs(float(cur[2])) >= 1e-30 :
            II.append( int(cur[0]) )
            JJ.append( int(cur[1]) )
            VV.append( float(cur[2]) )

    ## Build the coordinate sparse matrix and return it:
    return coo_matrix((VV, (II,JJ) ), shape = (ni,nj) )

def savematrix(matrix, filename):
    """
    Write scipy sparse matrix in FreeFEM format
    
    :filename: path to output file
    """

    ## Convert matrix to a coo matrix.
    try : matrix = matrix.tocoo()
    except : return False

    ## Extract matrix values and indices:
    data = matrix.data
    row, col = matrix.row, matrix.col

    ## Open file:
    with open(filename, 'w') as file:

        ## Write constant upper lines:
        file.write("#  HashMatrix Matrix (COO) 0x26ea510\n")
        file.write("#    n    m    nnz    half    fortran    state\n")

        ## Write problem size line:
        file.write(str(matrix.shape[0]) + " " + str(matrix.shape[1]) + " ")
        file.write(str(matrix.nnz) + " 0 0 0 0\n")

        ## Write matrix content:
        for ii in range(matrix.nnz) :
            file.write("         " + str(row[ii]) )
            file.write("         " + str(col[ii]) )
            file.write(" " + str(data[ii]) + "\n")

    ## Return True state:
    return True


# ====== I/O vector

def readdata(filename):
    """
    Read FreeFEM array file as numpy array
    
    :filename: path to source file
    """

    ## Open file:
    with open(filename,'r') as file:
        filecontent = file.readlines()

        ## Extract matix size:
        ext_size = filecontent[0].split()
        lin, col = int(ext_size[0]), int(ext_size[1])

        ## Prepare the array sturcture:
        VEC = np.empty((lin, col))

        ## Store array values:
        for ii in range(lin):
            cur = filecontent[ii+1].split()
            VEC[ii, : ] = list(map(float, cur))

    ## Return numpy vector:
    return VEC

def savedata(vector, filename):
    """
    Write numpy array in FreeFEM format
    
    :filename: path to output file
    """

    ## Open file:
    with open(filename, 'w') as file:

        ## Write problem size line:
        file.write( str(vector.shape[0]) + " " + str(vector.shape[1]) + "\n")

        ## Write matrix content:
        for ii in range(vector.shape[0]):
            file.write("       " + "   ".join(map(str, vector[ii, :])) + "\n")

    ## Return True state:
    return True
